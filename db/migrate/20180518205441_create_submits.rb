class CreateSubmits < ActiveRecord::Migration
  def change
    create_table :submits do |t|
      t.text :text
      t.string :source

      t.timestamps null: false
    end
  end
end
