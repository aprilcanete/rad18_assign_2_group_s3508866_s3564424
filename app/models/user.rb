class User < ActiveRecord::Base
    has_secure_password
    
    validates :username,
                presence: true,
                length: { minimum: 10, maximum: 15 },
                format: { with: /[a-zA-Z0-9_@-]/},
                uniqueness: { case_sensitive: false }
    
    validates :email,
                presence: true
    
    validates :password, length: { minimum: 10 }
end
