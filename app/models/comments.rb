class Comment < ActiveRecord::Base
    validates :message,
        presence: true,
        length: {minimum: 8, maximum: 1000},
        allow_blank: false
end
       