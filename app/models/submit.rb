class Submit < ActiveRecord::Base
    validates :text,
        presence: true,
        length: {minimum: 10, maximum: 200}
end
