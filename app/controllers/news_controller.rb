class NewsController < ApplicationController
    def index
        @newsall = News.all
    end
  
   # def new
   #     @newsstory = NewsStory.new
   # end
  
   # def create
    #    @newsstory = NewsStory.new(params[:newsstory ])
   #     if @newsstory.save
    #        redirect_to new_story_path
   # end
    
    def submit
        @news = News.new
    end

    def create
        @news = News.new(news_params)
    
        if @news.save
          redirect_to root_path, notice: 'News succesfully created!'
        else
          render :submit
        end
    end
    
    def home
        @news = News.all
    end
    
    def show
        @news = News.find(news_params)
    end
    
    def comments
    
    end
  
  private
  
  def news_params
   params.require(:news).permit(:title, :source, :author)
  end
  
end
