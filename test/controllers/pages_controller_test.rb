require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
  end

  test "should get comments" do
    get :comments
    assert_response :success
  end

  test "should get submit" do
    get :submit
    assert_response :success
  end

end
